#include "obj.h"
#include <assert.h>
#include <fstream>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>

int main()
{
	const char* fileName = "saved.txt";

	// Create one object o1.
	const Obj o1(-2, false);
	const Obj* const p1 = &o1;

	// Save data
	{
		// Create an output archive
		std::ofstream ofs(fileName);
		boost::archive::xml_oarchive ar(ofs);
		// Save only the pointer. This will trigger serialization
		// of the object it points too, i.e., o1.
		ar & BOOST_SERIALIZATION_NVP(p1);
	}

	// Restore data
	Obj* restored_p1;
	{
		// Create and input archive
		std::ifstream ifs(fileName);
		boost::archive::xml_iarchive ar(ifs);
		// Load
		ar & BOOST_SERIALIZATION_NVP(restored_p1);
	}

	// Make sure we read exactly what we saved.
	assert(restored_p1 != p1);
	assert(*restored_p1 == o1);

	return 0;
}