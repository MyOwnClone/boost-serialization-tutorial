#pragma once
// File SerializeCStringHelper.hpp

#include <string>
#include <boost/serialization/string.hpp>
#include <boost/serialization/split_member.hpp>

class SerializeCStringHelper {
public:
	SerializeCStringHelper(char*& s) : s_(s) {}
	SerializeCStringHelper(const char*& s) : s_(const_cast<char*&>(s)) {}

private:

	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive& ar, const unsigned version) const {
		bool isNull = (s_ == 0);
		ar & isNull;
		if (!isNull) {
			std::string s(s_);
			ar & s;
		}
	}

	template<class Archive>
	void load(Archive& ar, const unsigned version) {
		bool isNull;
		ar & isNull;
		if (!isNull) {
			std::string s;
			ar & s;
			s_ = strdup(s.c_str());
		}
		else {
			s_ = 0;
		}
	}

	BOOST_SERIALIZATION_SPLIT_MEMBER();

private:
	char*& s_;
};