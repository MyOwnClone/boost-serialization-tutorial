#include "cstringserializationhelper.h"
#include <assert.h>
#include <fstream>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

int main()
{
	const char* fileName = "saved.txt";
	const char* str = "This is an example a C-string";

	// Save data
	{
		// Create an output archive
		std::ofstream ofs(fileName);

		boost::archive::text_oarchive ar(ofs);
		// Save
		SerializeCStringHelper helper(str);
		ar & helper;
	}

	// Restore data
	char* restored_str;
	{
		// Create and input archive
		std::ifstream ifs(fileName);
		boost::archive::text_iarchive ar(ifs);

		// Load
		SerializeCStringHelper helper(restored_str);
		ar & helper;
	}

	// Make sure we read exactly what we saved
	assert(restored_str != str);
	assert(strcmp(restored_str, str) == 0);

	return 0;
}