#include "../boost-serialization-tutorial/obj.h"
#include <assert.h>
#include <fstream>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

int main()
{
	const char* fileName = "saved.txt";

	// Create one object o1.
	const Obj o1(-2, false);
	const Obj* const p1 = &o1;

	// Save data
	{
		// Create an output archive
		std::ofstream ofs(fileName);
		boost::archive::text_oarchive ar(ofs);
		// Save only the pointer. This will trigger serialization
		// of the object it points too, i.e., o1.
		ar & p1;
	}

	// Restore data
	Obj* restored_p1;
	{
		// Create and input archive
		std::ifstream ifs(fileName);
		boost::archive::text_iarchive ar(ifs);
		// Load
		ar & restored_p1;
	}

	// Make sure we read exactly what we saved.
	assert(restored_p1 != p1);
	assert(*restored_p1 == o1);

	return 0;
}