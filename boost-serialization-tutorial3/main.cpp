#include "../boost-serialization-tutorial/obj.h"
#include <fstream>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

int main()
{
	const char* fileName = "saved.txt";
	std::ofstream ofs(fileName);

	// Create one object o1 and a pointer p1 to that object.
	const Obj o1(-2, false);
	const Obj* const p1 = &o1;

	// Serialize object, then pointer.
	// This works fine: after the object is deserialized, we can
	// deserialize the pointer by assigning it to the object�s address.
	{
		boost::archive::text_oarchive ar(ofs);
		ar & o1 & p1;
	}

	// Serialize pointer, then object.
	// This does not work: once p1 has been serialized, the object
	// has already been deserialized and its address cannot change.
	// This will throw an instance of 'boost::archive::archive_exception'
	// at runtime.
	try
	{
		boost::archive::text_oarchive ar(ofs);
		ar & p1 & o1;
	}
	catch (boost::archive::archive_exception e)
	{
		std::cout << e.what() << std::endl;
	}

	return 0;
}