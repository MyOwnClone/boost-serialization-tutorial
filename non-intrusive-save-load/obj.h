#pragma once

#include <boost/serialization/split_free.hpp>

class Obj {
public:
	Obj() : d1_(-1), d2_(false) {}
	Obj(int d1, bool d2) : d1_(d1), d2_(d2) {}
	bool operator==(const Obj& o) const {
		return d1_ == o.d1_ && d2_ == o.d2_;
	}

public:
	int  d1_;
	bool d2_;
};

namespace boost {
	namespace serialization {

		template<class Archive>
		void save(Archive & ar, const Obj& o, const unsigned int version) {
			ar & o.d1_ & o.d2_;
		}

		template<class Archive>
		void load(Archive & ar, Obj& o, const unsigned int version) {
			ar & o.d1_ & o.d2_;
		}

	} // namespace serialization
} // namespace boost

BOOST_SERIALIZATION_SPLIT_FREE(Obj)