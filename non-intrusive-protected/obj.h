#pragma once

//// Declaration of the template
class Obj;

namespace boost {
	namespace serialization {

		template<typename Archive>
		void serialize(Archive& ar, Obj& o, const unsigned int version);

	} // namespace serialization
} // namespace boost

//// Definition of the class
class Obj {
public:
	Obj() : d1_(-1), d2_(false) {}
	Obj(int d1, bool d2) : d1_(d1), d2_(d2) {}
	bool operator==(const Obj& o) const {
		return d1_ == o.d1_ && d2_ == o.d2_;
	}

private:
	int  d1_;
	bool d2_;

	// Allow serialization to access data members.
	template<typename Archive> friend
		void boost::serialization::serialize(Archive& ar, Obj& o, const unsigned int version);
};

//// Definition of the template
namespace boost {
	namespace serialization {

		template<typename Archive>
		void serialize(Archive& ar, Obj& o, const unsigned int version) {
			ar & o.d1_ & o.d2_;
		}

	} // namespace serialization
} // namespace boost